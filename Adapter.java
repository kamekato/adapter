class KaspiBank {
	private int balance;
	public KaspiBank() { balance = 100; }
	public void getBalance() {
		System.out.println("KaspiBank balance = " + balance);
	}
}
class HalykBank {
	private int balance;
	public HalykBank() { balance = 200; }
	public void getBalance() {
		System.out.println("HalykBank balance = " + balance);
	}
}
class KaspiBankAdapter extends KaspiBank {
	private HalykBank halykbank;
	public KaspiBankAdapter(HalykBank halykbank) {
		this.halykabank = halykbank;
	}
	public void getBalance() {
		halykbank.getBalance();
	}
}

public class AdapterTest {
	public static void main(String[] args) {
		KaspiBank kaspibank = new KaspiBank();
		Kaspibank.getBalance();
		KaspiBankAdapter halykbank = new KaspiBankAdapter(new HalykBank());
		Halykbank.getBalance();
	}
}